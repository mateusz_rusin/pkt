<?php

namespace App;

use App\Handler\ProvisioningHandler;
use App\Handler\SubmitScoreHandler;
use App\Helper\PKTHelper;
use App\Helper\PKTNotification;
use App\Helper\PKTScoreCreator;
use Buzz\Browser;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class ControllerProvider implements ControllerProviderInterface
{
    /**
     * @ToDo match -> post
     * @param Application $app
     *
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        // PROVISIONING
        $controllers->match('/provisions/rest/reservation', function (Request $request) use ($app) {
            $handler = new ProvisioningHandler(
                $request,
                $app['validator'],
                $app['monolog'],
                $app->raw('config.custom.provisioning')
            );
            $handler->run();

            $response = $handler->response();

            return $response;
        });

        // SUBMIT
        $controllers->match('/scores/rest/submitScoreRequest', function (Request $request) use ($app) {
            $handler = new SubmitScoreHandler(
                $request,
                $app['validator'],
                $app['monolog'],
                $app->raw('config.custom.submitScore')
            );
            $handler->run();

            $response = $handler->response();

            return $response;
        });

        // Proxy for notification
        $controllers->match('/scores/rest/notification/', function (Request $request) use ($app)  {
            $pktNotification = new PKTNotification(
                $request->request->all(),
                $app->raw('config.custom.notification')
            );
            $pktNotification->prepare();

            $response = $pktNotification->send(PKTHelper::NOTIFICATION_DELAY);

            return $response;
        });

        return $controllers;
    }
}
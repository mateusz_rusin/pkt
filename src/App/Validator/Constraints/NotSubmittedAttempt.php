<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotSubmittedAttempt extends Constraint {

    public $message = 'The attempt with TestInstanceId: "%string%" is already submitted';
} 
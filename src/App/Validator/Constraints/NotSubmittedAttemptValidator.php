<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Model\PktInfo;

class NotSubmittedAttemptValidator extends ConstraintValidator {

    /**
     * @param string     $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $pktInfo = PktInfo::find_by_test_instance_id($value);

        if (!$pktInfo || $pktInfo->submitted) {
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
        }
    }
} 
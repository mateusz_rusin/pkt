<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidEntitlementId extends Constraint {

    public $message = 'The Entitlement ID: "%string%" is not valid';
} 
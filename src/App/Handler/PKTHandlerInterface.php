<?php

namespace App\Handler;

interface PKTHandlerInterface {

    /**
     * @return void
     */
    public function run();

    /**
     * @return JsonResponse
     */
    public function response();

    /**
     * @return void
     * @throws UnauthorizedHttpException
     */
    public function authorize();
} 
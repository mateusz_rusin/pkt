<?php

namespace App\Handler;

use App\Helper\PKTHelper;
use App\Helper\PKTResponseHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Validator\Validator;
use Symfony\Bridge\Monolog\Logger;

abstract class PKTHandler {

    /**
     * @var array
     */
    protected $data;

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $errors;

    /**
     * @var JsonResponse
     */
    protected $response;

    /**
     * @param Request    $request
     * @param Validator  $validator
     * @param Logger     $logger
     * @param array      $config
     */
    public function __construct(Request $request, Validator $validator, Logger $logger, array $config)
    {
        $this->data = json_decode($request->getContent(), true);

        $this->validator = $validator; //@todo remove provider?
        $this->logger = $logger;
        $this->config = $config;
        $this->pktResponseHelper = new PKTResponseHelper;
    }

    /**
     * @throws UnauthorizedHttpException
     */
    public function authorize()
    {
        /*
        $s = $_SERVER;
        $base64String = substr($_SERVER['HTTP_AUTHORIZATION'], 6);
        $base64Decoded = base64_decode($base64String);

        list($username, $password) = explode(':', $base64Decoded);
        */
        if ($_SERVER['PHP_AUTH_USER'] !== PKTHelper::AUTH_USERNAME || $_SERVER['PHP_AUTH_PW'] !== PKTHelper::AUTH_PASSWORD) {
            throw new UnauthorizedHttpException($_SERVER['PHP_AUTH_USER'].$_SERVER['PHP_AUTH_PW'], 'Not authorized');
        }
    }


    /**
     * @return JsonResponse
     */
    public function response()
    {
        return $this->response;
    }

    /**
     * @return bool
     */
    protected function validate()
    {
        $this->errors = $this->validator->validateValue($this->data, $this->getValidationConstraints());
        if (count($this->errors) > 0) {
            foreach ($this->errors as $error) {
                $this->logger->addError($error->getPropertyPath().' '.$error->getMessage());
            }

            return false;
        }

        return true;
    }

    /**
     *
     */
    protected function save()
    {

    }

    /**
     * @param array $data
     * @param int   $status
     * @param array $headers
     */
    protected function setResponse(array $data, $status = JsonResponse::HTTP_OK, $headers = array())
    {
        $this->response = new JsonResponse;
        $this->response->setData($data);

        if ($status) {
            $this->response->setStatusCode($status);
        }
        if ($headers) {
            $this->response->headers->add($headers);
        }
    }

    /**
     * @return string
     */
    protected function getStatus()
    {
        if (isset($this->config['status'])) {
            $status = $this->config['status'];
        } elseif (count($this->errors) > 0) {
            $status = PKTHelper::STATUS_FAIL;
        } else {
            $status = PKTHelper::STATUS_SUCCESS;
        }

        return $status;
    }

    /**
     * @return int
     */
    protected function getHttpStatus()
    {
        if (isset($this->config['httpStatus'])) {
            $status = $this->config['httpStatus'];
        } else {
            $status = JsonResponse::HTTP_OK;
        }

        return $status;
    }
} 
<?php

namespace App\Handler;

use App\Helper\PKTHelper;
use App\Helper\TestInstanceIdGenerator;
use App\Model\PktInfo;
use App\Validator\Constraints\ValidEntitlementId;
use Symfony\Component\Validator\Constraints as Assert;

class ProvisioningHandler extends PKTHandler implements PKTHandlerInterface {

    /**
     * @var string
     */
    private $testInstanceId;

    /**
     *
     */
    public function run()
    {
        $this->authorize();
        if ($this->validate()) {
            $this->save();
        }
        $this->prepareResponse();
    }

    /**
     *
     */
    protected function save()
    {
        $data = array(
            'entitlement_id' => $this->data[PKTHelper::REQUEST_KEY_NAME],
            'test_instance_id' => $this->getTestInstanceId(),
            'created' => date("Y-m-d H:i:s")
        );

        PktInfo::create($data);
    }

    /**
     *
     */
    protected function prepareResponse()
    {
        $status = $this->getStatus();

        $response = array(
            PKTHelper::RESPONSE_HEADER => $this->pktResponseHelper->prepareResponseHeader($status)
        );

        if ($status === PKTHelper::STATUS_SUCCESS) {
            $response[PKTHelper::RESPONSE_TEST_INSTANCE_ID] = $this->getTestInstanceId();
            $response[PKTHelper::RESPONSE_BRANDING_ID] = PKTHelper::RESPONSE_PKT;
            $response[PKTHelper::REQUEST_KEY_LOCALE] = PKTHelper::REQUEST_LOCALE_EN;
        } else {
            $response[PKTHelper::RESPONSE_HEADER][PKTHelper::RESPONSE_FAIL_DETAILS] = $this->pktResponseHelper->prepareFailDetails();
        }

        $this->setResponse($response, $this->getHttpStatus());
    }

    /**
     * @return Assert\Collection
     */
    protected function getValidationConstraints()
    {
        return new Assert\Collection(array(
            PKTHelper::REQUEST_KEY_NAME => array(
                new Assert\Length(array('min' => 19, 'max' => 19)),
                new ValidEntitlementId
            ),
            PKTHelper::REQUEST_KEY_LOCALE => array(
                new Assert\Type(array('type' => 'string')),
                new Assert\Length(array('min' => 2, 'max' => 2))
            ),
            PKTHelper::REQUEST_KEY_CUSTOMER_ID => new Assert\Length(array('min' => 3, 'max' => 3)),
            PKTHelper::REQUEST_KEY_REQUESTER_ID => new Assert\Length(array('min' => 4, 'max' => 4))
        ));
    }

    /**
     * @return string
     */
    private function getTestInstanceId()
    {
        if (!$this->testInstanceId) {
            $this->testInstanceId = TestInstanceIdGenerator::generate();
        }

        return $this->testInstanceId;
    }
} 
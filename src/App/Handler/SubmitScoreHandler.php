<?php

namespace App\Handler;

use App\Model\PktInfo;
use App\Helper\PKTHelper;
use App\Helper\SocketRequest;
use App\Validator\Constraints\NotSubmittedAttempt;
use Symfony\Component\Validator\Constraints as Assert;

class SubmitScoreHandler extends PKTHandler implements PKTHandlerInterface {

    /**
     * @var PktInfo
     */
    private $pktInfo;

    /**
     *
     */
    public function run()
    {
        $this->authorize();
        if ($this->validate()) {
            $this->update();
        }
        $this->prepareResponse();
    }

    /**
     *
     */
    protected function update()
    {
        $pktInfo = PktInfo::find_by_test_instance_id($this->data[PKTHelper::RESPONSE_TEST_INSTANCE_ID]);
        $pktInfo->submitted = date("Y-m-d H:i:s");
        $pktInfo->save();
    }

    /**
     *
     */
    protected function prepareResponse()
    {
        $status = $this->getStatus();

        $response = array(
            PKTHelper::RESPONSE_HEADER => $this->pktResponseHelper->prepareResponseHeader($status)
        );

        $this->setResponse($response, $this->getHttpStatus());
    }

    /**
     * @return Assert\Collection
     */
    protected function getValidationConstraints()
    {
        return new Assert\Collection(array(
            PKTHelper::RESPONSE_TEST_INSTANCE_ID => array(
                new Assert\NotBlank(),
                new Assert\Length(array('min' => 36, 'max' => 36)),
                new NotSubmittedAttempt
            ),
            PKTHelper::RESPONSE_ITEM_RESPONSES => new Assert\Type(array(
                'type' => 'array'
            )),
            PKTHelper::RESPONSE_NOTIFICATION_ENDPOINT => new Assert\NotBlank(),
            PKTHelper::RESPONSE_DELIVERY_METADATA => new Assert\Collection(array(
                PKTHelper::RESPONSE_LOCATION => new Assert\NotBlank()
            ))
        ));
    }

    /**
     *
     */
    private function notify()
    {
        $request = new SocketRequest;
        $request->setUrl($this->config['notify_url']);
        $request->setData(array(
            PKTHelper::RESPONSE_NOTIFICATION_ENDPOINT => $this->data[PKTHelper::RESPONSE_NOTIFICATION_ENDPOINT],
            PKTHelper::RESPONSE_TEST_INSTANCE_ID => $this->data[PKTHelper::RESPONSE_TEST_INSTANCE_ID]
        ));
        $request->send();
    }
} 
<?php

namespace App\Model;

class Model extends \ActiveRecord\Model
{
    const ENTITLEMENT = 'entitlement';
    const PKT_INFO = 'pkt_info';

    /**
     * @var array
     */
    protected static $belongs_to = array();

    /**
     * @param string $modelName
     *
     * @return Model
     */
    public static function factory($modelName)
    {
        $class = __NAMESPACE__.NS.ucfirst($modelName);

        return new $class;
    }

    /**
     * @return array
     */
    public function getSerializationParams()
    {
        $includes = array();

        foreach (static::$belongs_to as $table) {
            $includes[] = reset($table);
        }

        return array('include' => $includes);
    }
};

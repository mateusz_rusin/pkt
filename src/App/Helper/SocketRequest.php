<?php

namespace App\Helper;

class SocketRequest {

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $data;

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $post = array();
        foreach ($data as $key => &$val) {
            if (is_array($val)) {
                $val = implode(',', $val);
            }
            $post[] = $key.'='.urlencode($val);
        }
        $this->data = implode('&', $post);
    }

    /**
     *
     */
    public function send()
    {
        $parts = parse_url($this->url);

        $fp = fsockopen(
            $parts['host'],
            isset($parts['port']) ? $parts['port'] : 80,
            $errno,
            $errstr,
            30
        );

        $out = "POST ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($this->data)."\r\n";
        $out.= "Connection: Close\r\n\r\n";

        if (isset($this->data)) {
            $out .= $this->data;
        }

        fwrite($fp, $out);
        fclose($fp);
    }
}
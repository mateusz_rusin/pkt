<?php

namespace App\Helper;


class TestInstanceIdGenerator {

    const TEST_INSTANCE_ID_FORMAT = '/^(.{8})(.{4})(.{4})(.{4})(.{12})?/';

    /**
     * @return string
     */
    public static function generate()
    {
        $code = md5(microtime());
        $testInstanceId = self::prepare($code);

        return $testInstanceId;
    }

    /**
     * @param string $code
     *
     * @return string
     */
    private static function prepare($code)
    {
        preg_match(self::TEST_INSTANCE_ID_FORMAT, $code, $match);
        array_shift($match);

        return implode('-', $match);
    }
} 
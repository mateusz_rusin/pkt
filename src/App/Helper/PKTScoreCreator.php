<?php

namespace App\Helper;

use App\Helper\PKTHelper;
use App\Model\PktInfo;

class PKTScoreCreator {

    const SCORES_DIR = '../storage/score/';
    const SCORES_EXT = '.json';
    const DEFAULT_FILE = 'default.json';
    const EMPTY_JSON = '[{}]';

    /**
     * @var array
     */
    private $data;

    /**
     * @param array $data
     */
    function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return null|string
     */
    public function getScores()
    {
        $pktInfo = PktInfo::find_by_test_instance_id($this->data[PKTHelper::RESPONSE_TEST_INSTANCE_ID]);
        if ($pktInfo) {
            $file = $this->getFile($pktInfo->entitlement_id);

            return file_get_contents($file);
        }

        return self::EMPTY_JSON;
    }

    /**
     * @param $entitlementId
     *
     * @return string
     */
    private function getFile($entitlementId)
    {
        $file = self::SCORES_DIR.$entitlementId.self::SCORES_EXT;
        if (is_file($file)) {
            return $file;
        }

        return self::SCORES_DIR.self::DEFAULT_FILE;
    }









    /**
     * @return array
     */
    protected function getHeaderTemplate()
    {
        return array(
            PKTHelper::RESPONSE_NOTIFICATION_TYPE => null,
            PKTHelper::RESPONSE_NOTIFICATION_EVENT => null,
            PKTHelper::RESPONSE_TEST_INSTANCE_ID => null
        );
    }

    /**
     * @return array
     */
    protected function getScoresTemplate()
    {
        return array(
            PKTHelper::RESPONSE_SCORE_COMPONENT => null,
            PKTHelper::RESPONSE_TRAIT_LIST => array(
                PKTHelper::RESPONSE_TRAIT_COMPONENT => null,
                PKTHelper::RESPONSE_SCORE_VALUE => null,
                PKTHelper::RESPONSE_MIN_SCORE => null,
                PKTHelper::RESPONSE_MAX_SCORE => null
            )
        );
    }

    /**
     * @param array $item
     *
     * @return array
     */
    private function getTextScore(array $item)
    {
        return array(
            PKTHelper::RESPONSE_SCORE_COMPONENT => $item['itemId'],
            PKTHelper::RESPONSE_TRAIT_LIST => array(
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'CON',
                    PKTHelper::RESPONSE_SCORE_VALUE => 3.3,
                    PKTHelper::RESPONSE_MIN_SCORE => 0.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 5.0
                ),
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'PRON',
                    PKTHelper::RESPONSE_SCORE_VALUE => 1.0,
                    PKTHelper::RESPONSE_MIN_SCORE => -3.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 3.0
                ),
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'FLU',
                    PKTHelper::RESPONSE_SCORE_VALUE => 0.52,
                    PKTHelper::RESPONSE_MIN_SCORE => -3.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 3.0
                ),
            )
        );
    }

    /**
     * @param array $item
     *
     * @return array
     */
    private function getAudioScore(array $item)
    {
        return array(
            PKTHelper::RESPONSE_SCORE_COMPONENT => $item['itemId'],
            PKTHelper::RESPONSE_TRAIT_LIST => array(
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'HOL',
                    PKTHelper::RESPONSE_SCORE_VALUE => 3.3,
                    PKTHelper::RESPONSE_MIN_SCORE => 0.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 5.0
                ),
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'GUM',
                    PKTHelper::RESPONSE_SCORE_VALUE => 1.0,
                    PKTHelper::RESPONSE_MIN_SCORE => -3.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 3.0
                ),
                array(
                    PKTHelper::RESPONSE_TRAIT_COMPONENT => 'CON',
                    PKTHelper::RESPONSE_SCORE_VALUE => 0.52,
                    PKTHelper::RESPONSE_MIN_SCORE => -3.0,
                    PKTHelper::RESPONSE_MAX_SCORE => 3.0
                ),
            )
        );
    }
} 
<?php

namespace App\Helper;

use Buzz\Browser;

class PKTNotification {

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $config;

    /**
     * @var Browser
     */
    private $browser;

    /**
     * @param array $data
     * @param array $config
     */
    public function __construct(array $data, array $config)
    {
        $this->data = $data;
        $this->config = $config;
        $this->browser = new Browser;
    }

    /**
     *
     */
    public function prepare()
    {
        $this->browser->getClient()->setTimeout(PKTHelper::CLIENT_DELAY);
    }

    /**
     * @param int $delay
     *
     * @return \Buzz\Message\MessageInterface
     */
    public function send($delay)
    {
        sleep($delay);
        return $this->browser->post($this->getUrl(), array(), $this->getScores());
    }

    /**
     * @return string
     */
    private function getUrl()
    {
        $url = str_replace(
            array('{0}', '{1}'),
            array(PKTHelper::NOTIFICATION_USERNAME, PKTHelper::NOTIFICATION_PASSWORD),
            $this->data[PKTHelper::RESPONSE_NOTIFICATION_ENDPOINT]
        );

        return $this->config['url'].$url;
    }

    /**
     * @return string
     */
    private function getScores()
    {
        $pktScoreCreator = new PKTScoreCreator($this->data);

        return $pktScoreCreator->getScores();
    }
} 
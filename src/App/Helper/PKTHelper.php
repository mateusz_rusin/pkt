<?php

namespace App\Helper;

class PKTHelper {

    const REQUEST_KEY_NAME = 'name';
    const REQUEST_KEY_LOCALE = 'locale';
    const REQUEST_LOCALE_EN = 'en';
    const REQUEST_KEY_CUSTOMER_ID = 'customerId';
    const REQUEST_KEY_REQUESTER_ID = 'requesterId';

    const RESPONSE_HEADER = 'responseHeader';
    const RESPONSE_TEST_INSTANCE_ID = 'testInstanceId';
    const RESPONSE_BRANDING_ID = 'brandingId';
    const RESPONSE_PKT = 'pkt';
    const RESPONSE_FAIL_DETAILS = 'failDetails';
    const RESPONSE_ITEM_RESPONSES = 'itemResponses';
    const RESPONSE_ITEM_ID = 'itemId';
    const RESPONSE_TIMESTAMP = 'timestamp';
    const RESPONSE_ITEM_TYPE ='itemType';
    const RESPONSE_TEXT = 'text';
    const RESPONSE_CRC = 'crc32Checksum';
    const RESPONSE_NOTIFICATION_ENDPOINT = 'notificationEndpoint';
    const RESPONSE_DELIVERY_METADATA = 'deliveryMetadata';
    const RESPONSE_LOCATION = 'location';
    const RESPONSE_NOTIFICATION_TYPE = 'notificationType';
    const RESPONSE_NOTIFICATION_EVENT = 'notificationEvent';
    const RESPONSE_SCORE_DATA = 'scoreData';
    const RESPONSE_SCORE_COMPONENT = 'scoreComponent';
    const RESPONSE_TRAIT_LIST = 'traitList';
    const RESPONSE_TRAIT_COMPONENT = 'traitComponent';
    const RESPONSE_SCORE_VALUE = 'scoreValue';
    const RESPONSE_MIN_SCORE = 'minScore';
    const RESPONSE_MAX_SCORE = 'maxScore';

    const STATUS_FAIL = 'FAIL';
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_BUSY = 'BUSY';

    const AUTH_USERNAME = 'pktgSe';
    const AUTH_PASSWORD = 'n4t7l1A';

    const NOTIFICATION_USERNAME = 'pktGse';
    const NOTIFICATION_PASSWORD = 'm4aRfr5Ecd';

    const NOTIFICATION_DELAY = 15;
    const CLIENT_DELAY = 25;
} 
<?php

namespace App\Helper;


class PKTResponseHelper {

    /**
     * @param int $status
     *
     * @return array
     */
    public function prepareResponseHeader($status)
    {
        return array(
            'version' => '1.1',
            'status' => $status
        );
    }

    /**
     * @return array
     */
    public function prepareFailDetails()
    {
        return array(
            'errorType' => "Invalid"
        );
    }
} 
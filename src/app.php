<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use App\ControllerProvider;
use Ruckuus\Silex\ActiveRecordServiceProvider;

$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new ActiveRecordServiceProvider, $app->raw('config.activerecord'));
$app->register(new MonologServiceProvider, $app->raw('config.monolog'));

$app->mount('/', new ControllerProvider);

$app->error(function (\Exception $e, $code) use ($app) {
    $app['monolog']->addError($e->getMessage());
    $app['monolog']->addError($e->getTraceAsString());

    return new JsonResponse(array(
        "statusCode" => $code,
        "message" => $e->getMessage(),
        "stacktrace" => $e->getTraceAsString()
    ));
});